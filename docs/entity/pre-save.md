---
meta:
  - name: description
    content: Modify entities before they are being saved
  - name: keywords
    content: Drupal 8 entity presave
---

# Modify entities before they are being saved

Entities can be modified before saving using [hook_entity_presave](https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!entity.api.php/function/hook_entity_presave).

In following snippet, all authors are saved in a field named `field_authors`, which can then be used to display a list of all authors that made modifications to an entity.

```
.
└─ web
   └─ modules
      └─ custom
         └─ MY_MODULE
            ├─ MY_MODULE.info.yml
            └─ MY_MODULE.module
```

**MY_MODULE.module**

``` php
<?php

use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_entity_presave().
 */
function MY_MODULE_entity_presave(EntityInterface $entity) {
  // Only apply to content of type MY_BUNDLE.
  if ($entity->bundle() == 'MY_BUNDLE') {
    // Get previously added authors.
    $authorIds = array_map(
      function ($value) {
        return $value['target_id'];
      },
      $entity->field_authors->getValue()
    );

    // Get current author.
    $currentAuthorId = $entity->revision_uid->getValue()[0]['target_id'];

    // Add current author if not already in the list.
    if (!in_array($currentAuthorId, $authorIds)) {
      $entity->set(
        'field_authors',
        array_merge(
          $entity->field_authors->getValue(),
          $entity->revision_uid->getValue()
        )
      );
    }
  }
}
```
