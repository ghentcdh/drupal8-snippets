---
meta:
  - name: description
    content: Create custom modules
  - name: keywords
    content: Drupal 8 custom modules
---

# Create custom modules

The default behaviour of Drupal can be modified using custom modules. Examples of such modifications are [altering forms](/form/alter#hook_form_FORM_ID_alter) and [modifying an entity before it is saved](/entity/pre_save).

::: warning
Hooks are only registered when a module is installed. When a new hook is added to a module that is already installed, the module should thus be uninstalled and installed again.
:::

## Minimal setup

A custom module at least consists of a [.info.yml](https://www.drupal.org/docs/8/creating-custom-modules/let-drupal-8-know-about-your-module-with-an-infoyml-file) file containing general info about the project and a `.module` file containing the application logic in php code.

```
.
└─ web
   └─ modules
      └─ custom
         └─ MY_MODULE
            ├─ MY_MODULE.info.yml
            └─ MY_MODULE.module
```

**MY_MODULE.info.yml**
``` yml
name: 'Module name'
description: 'Module description'
package: 'Custom'

type: module
core: 8.x
```

**MY_MODULE.module**
``` php
<?php

// The application logic goes here.
```
