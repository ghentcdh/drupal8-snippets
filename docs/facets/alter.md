---
meta:
  - name: description
    content: Modify facets
  - name: keywords
    content: Drupal 8 facets search api alter
---

# Modify facets

[Facets](https://www.drupal.org/project/facets) is a Drupal module that can be used with [Search API](http://drupal.org/project/search_api) to enable facet navigation on views. See [this post](https://medium.com/@swappyp20/creating-a-faceted-search-view-in-drupal-8-using-the-search-api-modules-2b2604ad37f4) for information on how to set it up.

::: warning
If you're using Search API views, make sure to [disable views](https://git.drupalcode.org/project/facets/blob/8.x-1.x/README.txt#L54) cache when using facets for that view.
:::

## Only display published results

First, add the `Published` field to the search index. Results can then be filtered for users without permission using [hook_search_api_query_TAG_alter hook](https://git.drupalcode.org/project/search_api/blob/8.x-1.x/search_api.api.php#L364).

```
.
└─ web
   └─ modules
      └─ custom
         └─ MY_MODULE
            ├─ MY_MODULE.info.yml
            └─ MY_MODULE.module
```

**MY_MODULE.module**

``` php
<?php

use Drupal\search_api\Query\QueryInterface;

/**
 * Implements hook_search_api_query_TAG_alter().
 */
function MY_MODULE_search_api_query_views_VIEW_ID_alter(
  QueryInterface &$query
) {
  $user = \Drupal::currentUser();
  if (!$user->hasPermission('edit any MY_BUNDLE content')) {
    $query->addCondition('status', 1);
  }
}
```

## Use the same facets for multiple views on the same page

In some cases, one view should display paginated results (e.g., in a list with links), while another view displays all results (e.g., on a map).

The facet selection from one display can be applied to another display using [hook_search_api_query_TAG_alter hook](https://git.drupalcode.org/project/search_api/blob/8.x-1.x/search_api.api.php#L364):

```
.
└─ web
   └─ modules
      └─ custom
         └─ MY_MODULE
            ├─ MY_MODULE.info.yml
            └─ MY_MODULE.module
```

**MY_MODULE.module**

``` php
<?php

use Drupal\search_api\Query\QueryInterface;

/**
 * Implements hook_search_api_query_TAG_alter().
 */
function MY_MODULE_search_api_query_views_VIEW_ID_alter(
  QueryInterface &$query
) {
  if ($query->getSearchId() === 'views_block:VIEW_ID__DISPLAY_ID_WITHOUT_FACETS') {
    $facet_manager = \Drupal::service('facets.manager');
    $facet_manager->alterQuery($query, 'search_api:views_page__VIEW_ID__DISPLAY_ID_WITH_FACETS');
  }
}
```
