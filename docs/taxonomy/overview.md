---
meta:
  - name: description
    content: Modify taxonomy overview pages
  - name: keywords
    content: Drupal 8 taxonomy overview alter
---

# Modify taxonomy overview pages

The page where all terms of a vocabulary is listed, [is a form](https://git.drupalcode.org/project/drupal/blob/8.8.x/core/modules/taxonomy/src/Form/OverviewTerms.php). A [hook_form_FORM_ID_alter](/form/alter#hook_form_FORM_ID_alter) (with `taxonomy_overview_terms` as `FORM_ID`) can thus be used to modify this overview.

In following snippet, the value of an additional field (which is a reference to an other taxonomy term) is added to the name of each term in the overview.

```
.
└─ web
   └─ modules
      └─ custom
         └─ MY_MODULE
            ├─ MY_MODULE.info.yml
            └─ MY_MODULE.module
```

**MY_MODULE.module**

``` php
<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function MY_MODULE_form_taxonomy_overview_terms_alter(
  &$form,
  FormStateInterface $form_state,
  $form_id
) {
  if (
    $form_state->get('taxonomy') != null
    && isset($form_state->get('taxonomy')['vocabulary'])
    && $form_state->get('taxonomy')['vocabulary']->id() == 'MY_VOCABULARY_ID'
  ) {
    $referencedTerms = [];
    foreach ($form['terms'] as $key => $value) {
      if (substr($key, 0, 4) === 'tid:') {
        $referencedTermId = $value['#term']
                              ->get('field_MY_FIELD')
                              ->getValue()[0]['target_id'];
        if (!array_key_exists($referencedTermId, $referencedTerms)) {
          $referencedTerms[$chronLev1Id] = Term::load($referencedTermId);
        }
        $form['terms'][$key]['term']['#title'] =
          $value['#term']->getName()
          . ' - '
          . $referencedTerms[$referencedTermId]->getName();
      }
    }
  }
}
```
