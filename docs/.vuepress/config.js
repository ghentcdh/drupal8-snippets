module.exports = {
  title: 'Drupal 8 snippets',
  description: 'Pieces of code used in various Drupal 8 projects.',
  themeConfig: {
    sidebar: [
      '/module/',
      '/form/alter',
      '/entity/pre-save',
      '/taxonomy/overview',
      '/facets/alter',
    ],
    repo: 'https://gitlab.com/ghentcdh/drupal8-snippets',
  },
  base: '/drupal8-snippets/',
  dest: 'public',
}
