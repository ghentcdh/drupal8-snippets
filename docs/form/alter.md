---
meta:
  - name: description
    content: Modify forms
  - name: keywords
    content: Drupal 8 form alter
---

# Modify forms

Drupal forms can be modified using [hook_form_alter](https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Form!form.api.php/function/hook_form_alter) or [hook_form_FORM_ID_alter](https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Form!form.api.php/function/hook_form_FORM_ID_alter). These hooks can be added as application logic in a [custom module](/module).

## Finding FORM\_ID for hook\_form\_FORM\_ID\_alter

The FORM\_ID can be found by using `hook_form_alter`:

```
.
└─ web
   └─ modules
      └─ custom
         └─ MY_MODULE
            ├─ MY_MODULE.info.yml
            └─ MY_MODULE.module
```

**MY_MODULE.module**

``` php
<?php

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_alter().
 */
function MY_MODULE_form_alter(
  &$form,
  FormStateInterface $form_state,
  $form_id
) {
  var_dump($form_id);
}
```
